## 0707 開會
### 報告
- 把API部署到學校的server
- 子毅
    - 餐廳端使用API進行餐點編輯
- 美瑜
    - API調整
    - 餐廳端頁面的更新問題
    - 驗證信、忘記密碼的實作研究
- 容華
    - API調整
    - IIS server不能用PUT、DELETE等設定
    - 使用者驗證 
- 晴雯
    - 顧客的歷史訂單
    - 使用者設定UI(登出、account page)

### 建議
- sprint是一個階段不是主題
    - 如果有突發狀況也可以把task加進該sprint
- 不要以功能為主題
    - 每個sprint以產出一個MVP為目標
- AP的user profile跟實際情況可能不同
    - 看是要完全獨立或是混合用
    - user profile要存放什麼東西，資源占用問題
        - eg. 大頭貼、餐點偏好
- 每個禮拜還是要demo
- 或許可以把form based app拉進react-native裡面

### 實習
- Vincent看看可不可以用part time的方式到AP台灣公司打工