## 0302 晚上教學
- 建議
    - milestone快點拆分

- 問題
    - 楊子毅
        - 跳出子表單可能需要動到CSS和js，比較複雜，或是也可以用raw html去進行修改，按下按鈕後觸發auto lookup，再讓html顯示出來
    - 姜美瑜
        - 用篩選器去過濾不同的店家ID(${username})
    - 楊晴雯
        - 日期的format有問題，無法新增，有奇怪的格式，但目前無法重現

- 教學
    - build page
        - 像在編table，新增後可再新增widget(eg. content, media, card, layout)
        - 預設字形較少，要使用其他字形要去改CSS
        - 跑馬燈:image carousel
        - 卡片式介紹:profile card, image card，亦可在其中新增link
        - publish時可選擇是否留agilepoint的menu
        - 但要注意set as homepage後若沒有menu就不能回到原本的管理頁了
        - menu在library，可add menu item
        - 無法達成rwd (RWD可用CSS達成，但很複雜)
        - 是靜態頁面，無法根據不同登入者顯示不同的資訊

    - material design
        - 一種設計風格