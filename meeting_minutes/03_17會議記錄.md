## 0317 meeting
- 報告
    - 楊子毅
        - 建立新的app和eform
        - 優化已接訂單&歷史接單介面
    - 胡容華
        - 把餐廳跟商店合併在同一個頁面,因為資料太少 如果後續欄位有新增，會考慮作拆分
        - 確認完怎麼新增subform的資料會將訂單補完
    - 楊晴雯
        - 建立新的app和eform
        - 歷史訂單的search button配合0227&0309的教學影片做好了
        - 可以有只搜尋部分選項的功能嗎 例如只搜尋餐廳 商店、種類不選...
        - 搜尋可隱藏之後會用
    - 姜美瑜
        - 隱藏button

- UI FLOW
    - 將主要流程串起來
    - 約個時間把東西串起來

- 建議
    - 手機介面顯示問題
    - 要先建build page把主頁的連結用起來
    - 每個task一個issue，backlog建議放files裡
    - 調整Milestone的時間
    - 老師想要每個milestone完成後可以有link試試看做完的東西
        - 點開work center後會出現URL，可以整理成文件給老師
        - 做出landing page後才會比較符合一般user看到的東西
    - 簽證問題
