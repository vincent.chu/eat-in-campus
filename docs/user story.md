# Customer

I want to order meal early so that I can get the meal as soon as I arrive at the resuaurant.
想要提早點餐,讓我可以在到達餐廳時可以馬上拿到餐點
有事情或沒辦法出門,想要讓餐點能由外送員直接送到特定位置

## User Story - 查詢店家

**As a** customer. 

**I want to** 使用app查詢可訂餐的店家

**So I can** 不用出門就能選擇我要的店家

### Action Description

1. 顧客利用分類選擇店家
2. 點選進入店家的菜單頁面

### Required Functions

* 使用分類查詢店家
* 使用名稱排序
* 使用ranking排序

### Required Pages

* 店家查訊頁面
* 店家菜單頁面

## User Story - Customer Place Order 

**As a** customer. 

**I want to** use app order the meal and ask someone delivery it.

**So I can** have a meal without leaving my classroom or dorm.

### Action Description

1. Customer search for meal by category
2. Pick favorite meal from the list
3. Place order
4. 選擇是否需要外送
5. System record order to customer's favorite list 

### Required Functions

- Meal list by category
- Show meal detail
- Place order with meal id
- 選擇外送與否
- 使用評價牌籲

### Required Pages

- Meal list by category view
- Meal detail view
- Order comfirm page

## User Story - Customer Current Order List 
**As a** customer.

**I want to** see my current order list, and manage them in one page.

**So I can** clearly know each of my order is in what state(preparing or delivering, etc).

### Action Description

1. 打開未送訂單管理頁面
2. 利用分類查詢未送訂單
3. 點擊查看未送訂單
4. 查看未送訂單資訊

### Required Functions

* 未送訂單查詢
* 使用時間排序
* 使用店家排序
* 使用商品分類查詢
* 使用店家分類查詢
* 查詢未送訂單細項

### Required Pages

* 未送訂單查詢頁面
* 未送訂單細節頁面

## User Story - Cancle Order
**As a** customer.

**I want to** use management page to cancel my order.

**So I can** avoid mistake order.

### Action Description

1. 打開訂單管理頁面
2. 使用分類查詢未送訂單
3. 點擊查看訂單
4. 點選取消訂單
5. 系統通知店家取消訂單

### Required Functions

* 未送訂單查詢
* 查看未送訂單細項
* 使用時間排序
* 使用店家排序
* 使用商品分類查詢
* 使用店家分類查詢
* 取消未送訂單

### Required Pages

* 未送訂單查詢頁面
* 未送訂單細節頁面
* 取消未送訂單頁面

## User Story - Historical Order

**As a** customer.

**I want to** check my historical order list.

**So I can** know what I have bought before.

### Action Description

1. 打開訂單管理頁面
2. 使用分類查詢已送達訂單
3. 點擊查看訂單

### Required Functions

* 已送達訂單查詢
* 使用時間排序
* 使用店家排序
* 使用商品分類查詢
* 使用店家分類查詢
* 查看已送訂單細項

### Required Pages

* 已送訂單查詢頁面
* 已送訂單細節頁面

## User Story - Comment
**As a** customer.

**I want to** comment on restaurants or delivery men.

**So I can** express my feeling of certain using experience.

### Action Description

1. 打開訂單管理頁面
2. 使用分類查詢已送達訂單
3. 點擊查看訂單
4. 訂單結束後，點選評論
5. 評論店家與外送員
6. 確認評論送出

### Required Functions

* 已送訂單查詢
* 已送訂單分類
* 查看已送訂單細項
* 選擇評論
* 送出評論

### Required Pages

* 已送訂單查詢頁面
* 分類頁面
* 已送訂單細節頁面
* 評論頁面
* 確認評論送出頁面

# Delivery man

想藉由替別人送餐,賺點小外快或者交朋友

## User Story - 接單

**As a** delivery man.

**I want to** search for the order that hasn't been accepted.

**So I can** know what order I can deliver.

### Action Description

1. 打開未送訂單頁面
2. 選擇我想送餐的大樓(我等下上課的大樓)
3. 查看尚未被接收的訂單
4. 選擇想送的訂單
5. 放到已接收訂單列

### Required Functions

* 未送訂單查詢
* 未送訂單分類查詢
* 已接收訂單查詢

### Required Pages

* 未送訂單頁面
* 已接收訂單頁面

## User Story - 查看已接訂單//optinal

**As a** delivery man.

**I want to** see what orders that I accepted but haven't delivered.

**So I can** know what order I haven't delivered.

### Action Description

1. 選擇已接訂單頁面
2. 顯示訂單者姓名、餐點、送餐地點

### Required Functions

* 已接收訂單查詢

### Required Pages

* 已接收訂單頁面
* 訂單者的詳細資訊頁面

## User Story - 查看配送紀錄

**As a** delivery man.

**I want to** see what orders that I've delivered. 

**So I can** know how many that I've delivered. 

### Action Description

1. 打開已送餐歷史紀錄
2. 選擇時間軸
3. 查看配送紀錄

### Required Functions

* 查詢歷史配送紀錄
* 選擇時間軸

### Required Pages

* 歷史配送紀錄頁面

# Restaurant

想要提早收到客人的餐點,平均分配每個時段的工作量
想要收集客戶的歷史購買紀錄,分析客戶喜好

## User Story - check order

**As a** restaurant.

**I want to** check the order from customer.

**So I can** make the meal earlier.

### Action Description

1. Restaurant open order list page
2. Select the order
3. Check the order detail

### Required Functions

- Order list
- Show order detail

### Required Pages

- Order list page
- Order detail page

## User Story - accept order

**As a** restaurant.

**I want to** accept order.

**So I can** ~~make money~~.

### Action Description

1. Select the order
2. Check order detail
3. Accept the order
4. notify customer that the food is preparing

### Required Functions

- Order list
- Show order detail(including order id)
- Accept order button
- Notify customer

### Required Pages

- Order list page
- Order detail page

## User Story - confirm that the food is given to the delivery man/customer

**As a** restaurant.

**I want to** 點擊確認交貨按鈕.

**So I can** confirm the food is taken by the delivery man/customer.

### Action Description

1. Open Order detail Page
2. Click confirm shipment button
3. Notify customer that the food is delivering

### Required Functions

- Show order detail
- Confirm shipment button
- Notify customer

### Required Pages
- Order detail page

## User Story - reject order

**As a** restaurant.

**I want to** reject order.

**So I can** technically filter customer who has bad credit record.

### Action Description

1. Check order detail
2. Reject order
3. Notify the customer and delivery man that the order is rejected

### Required Functions

- Show order detail
- Reject order button
- Notify customer
- Notify delivery man

### Required Pages
- Order detail page

## User Story - prevent error happen

**As a** restaurant

**I want to** mark the sold out meal.

**So I can** prevent customer order the meal that was sold out.

### Action Description

1. open management menu page
2. select the sold-out meal
3. mark it as sold out

### Required Functions

- menu management
- let the meal be sold out

### Required Pages
- management menu page

## User Story - customized menu

**As a** restaurant

**I want to** make a customized menu

**So I can** change layout and theme of the menu

### Action Description

1. Open management menu page
2. Select customized menu
3. Start design menu

### Required Functions

- menu management
- customized menu

### Required Pages

- management menu page
- design menu page


## User Story - collect and analyze

**As a** restaurant

**I want to** collect customer's historical order.

**So I can** analyze consumer preferences.

### Action Description

1. open historical order list
2. click the analyze button
3. check it online or download it

### Required Functions

- historical order list
- analyze data
- visualize data
- download result

### Required Pages

- historical order list page
- analyze page

## User Story - add meal

**As a** restaurant

**I want to** add meal

**So I can** sell meal.

### Action Description

1. 開啟菜單管理頁面
2. 選擇新增餐點
3. 輸入餐點
4. 確認餐點送出
5. return to management menu page

### Required Functions

- Upload images of meal
- Set information of meal

### Required Pages

- Management menu page
- Create meal page
- Meal confirm page

## User Story - modify meal

**As a** restaurant

**I want to** modify meal

**So I can** rename or edit price

### Action Description

1. 開啟菜單管理頁面
2. 查詢餐點
3. 選擇修改的餐點
4. 修改餐點資訊
5. 確認送出
6. return to the management menu page

### Required Functions

- Upload images of meal
- modify information of meal

### Required Pages

- management menu page
- modify meal page
- Meal confirm page

## User Story - delete meal

**As a** restaurant

**I want to** delete meal

**So I can** not to sell this meal

### Action Description

1. 開啟菜單管理頁面
2. 查詢餐點
3. 選擇刪除的餐點
5. 確認刪除後送出送出
6. return to management menu page

### Required Functions

- delete meal

### Required Pages

- Management menu page
- Delete meal page
- Meal Confirm page

## User Story - add set

**As a** restaurant

**I want to** add set

**So I can** sell the set of meal to customer 

### Action Description

1. Into management menu page
2. push the add set button
3. choose the meal and add to the set
4. set the price of the set
5. push complete button to finish
6. return to management menu page

### Required Functions

- add meal
- delete meal
- set the price

### Required Pages

- management menu page
- add set page
- set confrim page

## User Story - modify set

**As a** restaurant

**I want to** modify the set of meal.

**So I can** modify the price or meal of the set

### Action Description

1. Into management menu page
2. Search and choose the set
3. push the modify set button
4. modify the meal of the set if you want
5. modify the price of the set if you want
6. push complete button to finish
7. return the management menu page

### Required Functions

- add meal
- delete meal
- set the price

### Required Pages

- management menu page
- modify set page
- set confrim page

## User Story - delete set

**As a** restaurant

**I want to** delete set

**So I can** not to sell this delete

### Action Description

1. Into management menu page
2. search and choose the set
3. push the delete set button
4. return the management menu page

### Required Functions

- delete set

### Required Pages

- management menu page
- delete set page
- delete confrim page

#  Foodcourt manager

想要收集客戶反饋,對店家進行管理
想要方便去看各店家的營業狀況,決定是否繼續與店家合作

## User Story - search the restaurant

**As a** foodcourt manager

**I want to** look the specific restaurant

**So I can** find the restaurant that I want to see quickly

### Action Description

1. Foodcourt manager open the page
2. Use filter(ex. name) to search the restaurant
3. Choose the search result and into the restaurant detail page

### Required Functions

- filter
- search the restaurant

### Required Pages

- restaurant list
- restaurant detail information view

## User Story - see comments

**As a** foodcourt manager

**I want to** see comments from customers.

**So I can** score the restaurants, and concern the restaurant that got lower score.

### Action Description

1. Foodcourt Manager open the page.
2. Choose the restaurant that you want to look
3. Choose comment button
4. Into the commnt page

### Required Functions

- search the restaurant
- manage comment

### Required Pages

- restaurant list
- restaurant detail information view
- restaurant comment list

## User Story - see revenue

**As a** foodcourt manager

**I want to** see revenue of each restaurant.

**So I can** decide whether cooperate with specific restaurant or not.

### Action Description

1. Foodcourt Manager open the page.
2. Choose the restaurant that you want to see
3. Choose revenue button
4. Into the revenue page

### Required Functions

- search the restaurant
- manage the revenue
- show the revenue as a graph

### Required Pages

- restaurant list
- restaurant detail information view
- restaurant revenue information
