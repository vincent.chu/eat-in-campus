### User Story

**As a** User 

**I want to** Have clear view of tasks which I need to give approval 

**So I can** Get things done quick without missing important applications